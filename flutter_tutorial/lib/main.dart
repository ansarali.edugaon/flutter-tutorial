import 'package:flutter/material.dart';
import 'package:flutter_tutorial/app_screens/Home.dart';
import 'package:flutter_tutorial/app_screens/MyButtons.dart';

void main() => runApp(MaterialApp(
  debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Long List"),
        ),
        body: MyHome(),
      ),
    ));

List<String> getListElement(){
  var items = List<String>.generate(25, (index) => "Item $index");
  return items;
}

Widget getListView(){
  var listItems = getListElement();
  var listView = ListView.builder(
    itemBuilder: (context, index) {
      return ListTile(
        title: Text(listItems[index]),
        leading: Icon(Icons.account_balance_outlined),
        trailing: Icon(Icons.monetization_on),
        onTap: (){
          debugPrint(" Click on item $index");
        },
      );
    },
  );
  return listView;
}
