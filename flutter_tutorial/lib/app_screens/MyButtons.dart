import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "My Home",
      home: ButtonsData(),
    );
  }
}

class ButtonsData extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepOrange,
        title: Text("Tutorial"),
        elevation: 6.0,
      ),
      body: MyButton(),
    );
  }
}

class MyButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return
      Center(
        child: Container(
          height: 50.0,
          width: 150.0,
          // ignore: deprecated_member_use
          child: RaisedButton(
              color: Colors.blue,
              elevation: 6.0,
              textColor: Colors.white,
              child: Text("Click Me",
                style: TextStyle(
                    fontSize: 20.0
                ),),
              onPressed: () {
                pressButton(context);
              }
          ),
        ),);
  }

  void pressButton(BuildContext context) {
    var alertDialog = AlertDialog(
      title: Text("My Alert"),
      content: Text("This is my alert dialog"),
    );
    showDialog(context: context, builder: (BuildContext context) {
      return alertDialog;
    }
    );
  }
}
