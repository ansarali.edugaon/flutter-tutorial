import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
            // height: 500.0,
            // width:150.0,
            //margin: EdgeInsets.all(25.0),
            alignment: Alignment.center,
            color: Colors.blueAccent,
            padding: EdgeInsets.all(30.0),
            child: Column(
              children: [
                Row(
                  children: [
                    Expanded(
                        child:
                        Text(
                      "Patna",
                      textDirection: TextDirection.ltr,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 30.0,
                          fontStyle: FontStyle.italic,
                          decoration: TextDecoration.none),
                    )
                    ),
                    Expanded(
                        child: Text(
                      "Capital of  Bihar",
                      textDirection: TextDirection.ltr,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                          fontStyle: FontStyle.italic,
                          decoration: TextDecoration.none),
                    )
                    ),
                  ],
                ),
                Row(
                  children: [
                    Expanded(
                        child: Text(
                          "New Dehli",
                          textDirection: TextDirection.ltr,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 30.0,
                              fontStyle: FontStyle.italic,
                              decoration: TextDecoration.none),
                        )
                    ),
                    Expanded(
                        child: Text(
                          "Capital of  India",
                          textDirection: TextDirection.ltr,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20.0,
                              fontStyle: FontStyle.italic,
                              decoration: TextDecoration.none),
                        )),
                  ],
                ),
               Expanded(
                   child:MyImage()
               )
              ],
            )
        )
    );
  }
}

class MyImage extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    AssetImage assetImage = AssetImage("images/flutter.jpg");
    Image image = Image(image: assetImage);
    return Container(child: image,);
  }
}
